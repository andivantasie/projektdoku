<?php
declare(strict_types=1);

namespace EsearcherExt\JobToLatex;

use Esearcher\Extension\Installer\ExtensionInstaller;
use Exception;

/**
 * Class Installer
 *
 * @package EsearcherExt\JobToLatex
 */
class Installer extends ExtensionInstaller
{
    /**
     * @throws Exception
     */
    public function install() {
        $this->run("ALTER TABLE `job_template` ADD `ext_latex_template` VARCHAR( 255 ) NOT NULL");
        $this->run("UPDATE `job_template` SET `ext_latex_template` = 'template-one.tex.twig' WHERE `job_template`.`id` = 1");
    }

    /**
     * @throws Exception
     */
    public function uninstall() {
        $this->run("ALTER TABLE `job_template` DROP `ext_latex_template`");
    }
}