<?php
declare(strict_types=1);

namespace EsearcherExt\JobToLatex\src\Controller;

use Esearcher\Library\Action\Exception\ParameterNotFoundException;
use Esearcher\Library\Action\RouteParametersProxy;
use Esearcher\Library\DataMapper\Factory\MapperInterface;
use Esearcher\Model\Admin\AdminDocument;
use Esearcher\Model\Admin\Mapper\AdminDocumentMapper;
use Esearcher\Model\Project\Job;
use Esearcher\Model\Project\JobTemplate;
use Esearcher\Model\Project\Mapper\JobMapper;
use Esearcher\Model\Project\Mapper\JobTemplateMapper;
use EsearcherExt\JobToLatex\src\Service\HtmlToLatexConverter;
use Hrtk\Controller\ExtensionController;
use Hrtk\DbMapper\Mapper\MapperException;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\Runtim%\footnote{Schnittstelle, mit der fremder Programmcode in eine bestehende Anwendung integriert werden kann.~\cite{hook}}eError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

/**
 * Class Controller
 *
 * @package EsearcherExt\JobToLatex\src\Controller
 */
class Controller extends ExtensionController
{
    /**
     * @var FilesystemLoader
     */
    private $loader;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var JobMapper
     */
    private $jobMapper;

    /**
     * @var HtmlToLatexConverter
     */
    private $htmlToLatexConverter;

    /**
     * @var AdminDocumentMapper
     */
    private $adminDocumentMapper;

    /**
     * @var JobTemplateMapper
     */
    private $jobTemplateMapper;

    public function __construct(
        MapperInterface $mapper,
        FilesystemLoader $loader,
        HtmlToLatexConverter $htmlToLatexConverter
    ) {
        parent::__construct();
        $this->htmlToLatexConverter = $htmlToLatexConverter;
        $this->jobMapper = $mapper->get(JobMapper::class);
        $this->adminDocumentMapper = $mapper->get(AdminDocumentMapper::class);
        $this->jobTemplateMapper = $mapper->get(JobTemplateMapper::class);
        $this->loader = $loader;
        $this->loader->setPaths($this->viewPath);
        $this->twig = new Environment($this->loader);
    }

    /**
     * @param RouteParametersProxy $routeParametersProxy
     *
     * @throws LoaderError
     * @throws MapperException
     * @throws ParameterNotFoundException
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderJobLatexAction(RouteParametersProxy $routeParametersProxy): void
    {
        $jobId = (int)$routeParametersProxy->offsetGet('jobId');
        $job = $this->jobMapper->read($jobId);
        $this->htmlToLatexConverter->convert($job);

        /** @var JobTemplate $jobTemplate */
        $jobTemplate = $this->jobTemplateMapper->read((int)$job->getFormId());

        $latexTemplate = 'template-one.tex.twig';
        if ($jobTemplate instanceof JobTemplate) {
            $latexTemplate = $jobTemplate->getProp('extLatexTemplate');
        }

        echo $this->twig->render("/latex/{$latexTemplate}", ['job' => $job]);
    }

    /**
     * @param RouteParametersProxy $routeParametersProxy
     *
     * @throws MapperException
     * @throws ParameterNotFoundException
     */
    public function offerHeaderImageAction(RouteParametersProxy $routeParametersProxy)
    {
        $jobId = (int)$routeParametersProxy->offsetGet('jobId');
        $host = $_SERVER['HTTP_HOST'];

        /** @var Job $job */
        $job = $this->jobMapper->read($jobId);
        $jobHeaderImageId = (int)$job->getProp('jobHeaderImage');

        /** @var AdminDocument $adminDocument */
        $adminDocument = $this->adminDocumentMapper->read($jobHeaderImageId);

        $request = "https://{$host}/system/file/{$adminDocument->getExternalName()}";

        header("Location: {$request}");
    }

    /**
     * @param RouteParametersProxy $routeParametersProxy
     *
     * @throws ParameterNotFoundException
     */
    public function callApiAction(RouteParametersProxy $routeParametersProxy)
    {
        $jobId = (int)$routeParametersProxy->offsetGet('jobId');
        $host = $_SERVER['HTTP_HOST'];

        preg_match('/([\S]+HR4YOU_ESEARCHER[^\s;]+)/', $_SERVER['HTTP_COOKIE'], $matches);
        $cookie = $matches[0];

        $apiHost = 'v2202102142428144658.hotsrv.de';
        $apiRoute = 'api/showpdf';
        $request = "http://{$apiHost}/{$apiRoute}/{$host}/{$cookie}/{$jobId}";

        header("Location: {$request}");
    }
}
