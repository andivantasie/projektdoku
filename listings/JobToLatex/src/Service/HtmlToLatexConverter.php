<?php
declare(strict_types=1);

namespace EsearcherExt\JobToLatex\src\Service;

use Esearcher\Model\Project\Job;

/**
 * Class JobToLatexConverter
 *
 * @package EsearcherExt\JobToLatex\src\Service
 */
class HtmlToLatexConverter
{
    public function convert(Job $job): Job
    {
        $advertisementCompanyProfile = $job->getAdvertisementCompanyProfile();
        $job->setAdvertisementCompanyProfile($this->htmlToLatex($advertisementCompanyProfile));

        $advertisementIntroduction = $job->getAdvertisementIntroduction();
        $job->setAdvertisementIntroduction($this->htmlToLatex($advertisementIntroduction));

        $quantityAssignments = $job->getQuantityAssignments();
        $job->setQuantityAssignments($this->htmlToLatex($quantityAssignments));

        $quantityRequirements = $job->getQuantityRequirements();
        $job->setQuantityRequirements($this->htmlToLatex($quantityRequirements));

        $advertisementBenefits = $job->getAdvertisementBenefits();
        $job->setAdvertisementBenefits($this->htmlToLatex($advertisementBenefits));

        $advertisementEnding = $job->getAdvertisementEnding();
        $job->setAdvertisementEnding($this->htmlToLatex($advertisementEnding));

        return $job;
    }

    public function htmlToLatex(string $html): string
    {
        $allowedTags = '<ul><ol><li><strong><i><u>';
        $cleanedHtml = strip_tags($html, $allowedTags);

        $patterns = [
            '/<ul>/',
            '/<\/ul>/',
            '/<ol>/',
            '/<\/ol>/',
            '/<li>/',
            '/<\/li>/',
            '/<strong>/',
            '/<\/strong>/',
            '/<i>/',
            '/<\/i>/',
            '/<u>/',
            '/<\/u>/',
            '/%/'
        ];

        $replacements = [
            '\begin{itemize}',
            '\end{itemize}',
            '\begin{enumerate}',
            '\end{enumerate}',
            '\item ',
            '',
            '\textbf{',
            '}',
            '\textit{',
            '}',
            '\underline{',
            '}',
            '\%'
        ];

        return preg_replace($patterns, $replacements, $cleanedHtml);
    }
}
