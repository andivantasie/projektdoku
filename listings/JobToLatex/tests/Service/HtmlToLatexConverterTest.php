<?php
declare(strict_types=1);

namespace EsearcherExt\JobToLatex\tests\Service;

use EsearcherExt\JobToLatex\src\Service\HtmlToLatexConverter;
use PHPUnit\Framework\TestCase;

class HtmlToLatexConverterTest extends TestCase
{
    /**
     * @return array
     */
    public function htmlToLatexProvider(): array
    {
        return [
            'remove divs' => ['<div></div>', ''],
            'bold'        => ['<strong>Test</strong>', '\textbf{Test}'],
            'italics'     => ['<i>Test</i>', '\textit{Test}'],
            'itemize'     => ['<ul><li>Test</li></ul>', '\begin{itemize}\item Test\end{itemize}'],
            'enumerate'   => ['<ol><li>Test</li></ol>', '\begin{enumerate}\item Test\end{enumerate}']
        ];
    }

    /**
     * @dataProvider htmlToLatexProvider
     *
     * @param string $html
     * @param string $latex
     */
    public function testHtmlToLatex(string $html, string $latex): void
    {
        $htmlLatexConverter = new HtmlToLatexConverter();
        $actual = $htmlLatexConverter->htmlToLatex($html);

        $this->assertEquals($latex, $actual);
    }
}
