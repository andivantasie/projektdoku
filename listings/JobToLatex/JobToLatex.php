<?php
declare(strict_types=1);

namespace EsearcherExt\JobToLatex;

use Esearcher\Library\RouteConfigImp;
use Esearcher\Model\Project\Mapper\JobTemplateMapper;
use EsearcherExt\JobToLatex\src\Controller\Controller;
use Hrtk\Extension\Extension;

/**
 * Class JobToLatex
 *
 * @package EsearcherExt\JobToLatex
 */
class JobToLatex extends Extension
{
    public function getRoutes(): array
    {
        return [
            $this->createRoute(
                '/job-latex/{jobId}',
                [Controller::class, 'renderJobLatexAction'],
                'job-to-latex',
                (int)LEVEL_ARBEITGEBER,
                ['GET'],
                ['jobId' => RouteConfigImp::REGEX_AT_LEAST_ONE_DIGIT]
            ),
            $this->createRoute(
                '/job-header-image/{jobId}',
                [Controller::class, 'offerHeaderImageAction'],
                'always-accessible',
                0,
                ['GET'],
                ['jobId' => RouteConfigImp::REGEX_AT_LEAST_ONE_DIGIT]
            ),
            $this->createRoute(
                '/job-latex/callapi/{jobId}',
                [Controller::class, 'callApiAction'],
                'always-accessible',
                0,
                ['GET'],
                ['jobId' => RouteConfigImp::REGEX_AT_LEAST_ONE_DIGIT]
            )
        ];
    }

    public function getViewEventListeners(): array
    {
        return [
            $this->createViewEventListener(
                'views/default/controller/job/tab/facts:beforeButtonJobPreview',
                $this->viewsPath . 'default/controller/job/tab/facts/before-button-job-preview'
            )
        ];
    }

    public function getDbMapperExtensions(): array
    {
        return [
            $this->createDbMapperExtension(
                JobTemplateMapper::class,
                $this->schemaPath . 'jobTemplateMapper.json'
            ),
        ];
    }
}
