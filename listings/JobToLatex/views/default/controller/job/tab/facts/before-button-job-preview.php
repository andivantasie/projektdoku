<?php
declare(strict_types=1);

use Esearcher\Form\Job\Listing\FactsFormData;
use Esearcher\Model\Project\Job;

/** @var FactsFormData $factsFormData */
$factsFormData = $this->form->getFormData();
/** @var Job $job */
$job = $factsFormData->getModel('job');
?>
<a class="btn" href="/job-latex/callapi/<?= $job->getId() ?>" target="_blank">
    <?= dgettext('JobToPdf', 'PDF generieren') ?>
</a>
