<?php
$this->extend('lib/tab/container');
$this->begin('tabContent');
echo $this->form->open();
?>
<?php if ($this->dataId == 'newJob') {
    ?>
<input type="hidden" name="newJob" value="1">
<?php
}?>
    <div class="row">
        <div class="col-xs-12 col-md-4 col-lg-3">
            <div class="row">
                  <div id="blockStellenInformation" class="col-sm-4 col-md-12">
            <div class="panel panelForm">
                <input type="hidden" name="aktionId" value="<?php echo $this->urlParams['aktionId']; ?>" />
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo _('Stellen-Information'); ?></h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?php echo $this->form['jobKind']->label(_('Stellenart')); ?>
                            <?php echo $this->form['jobKind']; ?>
                        </div>
                    </div>
                    <div class="row">
                        <?php $this->hook('afterJobKind', array('form' => $this->form)); ?>
                        <div class="form-group col-xs-12 col-md-8 col-lg-12">
                            <?php echo $this->form['zipCode']->label(_('PLZ')); ?>
                            <?php echo $this->form['zipCode']; ?>
                        </div>
                        <div class="form-group col-xs-12">
                            <?php echo $this->form['location']->label(_('Arbeitsort')); ?>
                            <?php echo $this->form['location']; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?php
                            echo $this->form['country']->label(_('Land'));
                            echo $this->form['country'];
                            ?>
                        </div>
                        <div class="form-group col-xs-12">
                            <?php
                            echo $this->form['region']->label(_('Region'));
                            echo $this->form['region'];
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div id="blockInterneDaten" class="col-sm-8 col-md-12">
                <div class="panel panelForm">
                    <input type="hidden" name="aktionId" value="<?php echo $this->urlParams['id']; ?>" />
                    <div class="panel-heading">
                        <h4 class="panel-title"><?php echo _('Interne Daten'); ?></h4>
                    </div>
                    <div class="panel-body">
                                <div class="row">
                                    <div class="form-group col-xs-12">
                                        <?php echo $this->form['project']->label(_('Projekt')); ?>
                                        <div class="input-group">
                                        <?php echo $this->form['project']; ?>
                                        <?php echo $this->form['projectId']; ?>
                                        <span class="input-group-btn">
                                            <?php if(!empty($this->projectId)){ ?>
                                                <a class="btn" href="/project/<?php echo $this->projectId; ?>" target="_blank"><span class="open-icon"></span></a>
                                            <?php } ?>
                                            <button class="btn jobSearchProjectButton"
                                                    type="button"
                                                    widget="esearcher.search_project-for-jobs"
                                                    data-popup="false"
                                                    data-search-link="/dialog/search/jobproject/<?php echo $this->dataId; ?>"
                                                    data-dialog-button-ok-title="<?php echo _('OK'); ?>"
                                                    data-dialog-title="<?php echo _('Projektsuche') ?>"
                                                    title="<?php echo _('Projektsuche') ?>"
                                            >
                                                <span class="searchIcon"></span>
                                            </button>
                                        </span>
                                        </div>
                                        <a href="#"
                                           class="jobSearchProjectButton"
                                           title="<?php echo _('Projektsuche'); ?>"
                                           onclick="popupWindow('popup_projekt_search.php?from=job_edit', 'Projektsuche'); return false;"
                                        ></a>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php if (MODUL_MULTILANGUAGE){ ?>
                                    <div class="form-group col-xs-12 col-sm-4 col-md-12">
                                        <?php echo $this->form['language']->label(); ?>
                                        <?php echo $this->form['language']; ?>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group col-xs-12 col-sm-8 col-md-12">
                                        <?php echo $this->form['template']->label(); ?>
                                        <?php echo $this->form['template']; ?>
                                    </div>
                                    <?php $this->hook('internalTemplate', ['form' => $this->form]); ?>
                                    <?php $this->hook('applicantRegisterCvAnalyzer', array('form' => $this->form)); ?>
                                    <?php $this->hook('fieldHeaderImage', array('form' => $this->form)); ?>
                                </div>
                                 <div class="row">
                                    <div class="form-group col-xs-12">
                                        <?php echo $this->form['applicationFormSet']->label(); ?>
                                        <?php echo $this->form['applicationFormSet']; ?>
                                    </div>
                                </div>
                                <?php $this->hook('internalApplicationFormSet', ['form' => $this->form]); ?>
                                <div class="row">
                                    <div class="form-group col-xs-12">
                                        <label>
                                            <?= $this->form['active']->label() ?>
                                            <?php echo $this->form['active']; ?>
                                        </label>
                                    </div>
                                </div>
                                <input type="hidden" name="aktionId" value="<?php echo $this->urlParams['aktionId'];?>" />
                                <label><?= _('Angebotsdauer') ?>
                                    <?php $this->hook('jobOfferDuration', ['form' => $this->form]); ?>
                                </label>
                                <div class="row">
                                    <div class="form-group col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><?php echo _('Von'); ?></span>
                                            <?php
                                            echo $this->form['vonDatum']->getDateHtml();
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-6 col-md-12 col-lg-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><?php echo _('Bis'); ?></span>
                                            <?php
                                            echo $this->form['bisDatum']->getDateHtml();
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php $this->hook('internalDuration', ['form' => $this->form]); ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label>
                                            <?php //echo _('Bewerbungsformulartyp'); ?>
                                            <?php //echo $this->form['formulartyp0'] . $this->form['formulartyp0']->label(_('Standard')); ?>
                                            <?php //echo $this->form['formulartyp1'] . $this->form['formulartyp1']->label(_('CV-Autocomplete')); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-xs-12">
                                        <?php
                                        echo $this->form['informationen']->label(_('Informationen'));
                                        echo $this->form['informationen'];
                                        ?>
                                    </div>
                                </div>
                    </div>
                </div>
            </div>
                <?php $this->hook('afterInternalData', array('form' => $this->form)); ?>
            </div>
        </div>
        <div id="blockAngebotstext" class="col-xs-12 col-md-8 col-lg-9">
            <div class="panel panelForm">
                <input type="hidden" name="aktionId" value="<?php echo $this->urlParams['aktionId']; ?>" />
                <div class="panel-heading center-panel-heading">
                    <h4 class="panel-title"><?php echo _('Angebotstext'); ?></h4>
                    <div class="panel-tools">
                    <?php if ($this->dataId !== 'newJob') { ?>
                        <?php $this->hook('beforeButtonJobPreview', ['form' => $this->form]); ?>
                        <a id="previewJobBtn" class="btn" widget="esearcher.job_job-preview" data-lang="<?php echo $this->lang; ?>" data-job-id="<?php echo $this->dataId; ?>" data-title-fallback="<?php echo _('Vorschau Stellenangebot'); ?>"><?php echo _('Vorschau') ?></a>
                        <a class="btn" href="<?= $this->__host; ?>job/view/<?php echo $this->dataId; ?>" title="<?php echo _('Stellenangebot aufrufen'); ?>" target="_blank" ><?= _("Direktlink"); ?></a>
                        <?php $this->hook('jobTextPanelTools', ['job' => $this]) ?>
                    <?php } ?>
                    </div>
                </div>
                <div class="panel-body">
                     <div class="row">
                        <div class="form-group col-xs-12 col-lg-12">
                            <?php echo $this->form['jobDescription']->label(_('Stellenbezeichnung')); ?>
                            <?php echo $this->form['jobDescription']; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?php echo $this->form['companyDescription']->label(_('Firmenbeschreibung')); ?>
                            <?php echo $this->form['companyDescription']; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?php echo $this->form['description']->label((ENABLE_EXTENDED_JOB_EDITORS === "1") ? _('Zusammenfassung') : _('Stellenbeschreibung')); ?>
                            <?php echo $this->form['description']; ?>
                        </div>
                    </div>
                    <?php // dd(ENABLE_EXTENDED_JOB_EDITORS); ?>
                    <?php if(ENABLE_EXTENDED_JOB_EDITORS === "1"){ ?>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <?php echo $this->form['quantityAssignments']->label(_('Aufgaben')); ?>
                                <?php echo $this->form['quantityAssignments']; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <?php echo $this->form['quantityRequirements']->label(_('Anforderungen')); ?>
                                <?php echo $this->form['quantityRequirements']; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <?php echo $this->form['benefits']->label(_('Benefits')); ?>
                                <?php echo $this->form['benefits']; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <?php echo $this->form['ending']->label(_('Ausleitung')); ?>
                                <?php echo $this->form['ending']; ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?= $this->form->customerFields() ?>
<?php
echo $this->form->close();
$this->end();
