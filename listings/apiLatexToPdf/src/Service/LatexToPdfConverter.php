<?php
declare(strict_types=1);

namespace App\Service;

use Exception;

/**
 * Class LatexToPdfConverter
 *
 * @package App\Service
 */
class LatexToPdfConverter
{
    /**
     * @param string $fileName
     * @param string $latexCode
     *
     * @return string
     * @throws Exception
     */
    public function convert(string $fileName, string $latexCode): string
    {
        file_put_contents("{$fileName}.tex", $latexCode);

        exec("pdflatex {$fileName}.tex");
        exec("rm {$fileName}.aux");
        exec("rm {$fileName}.log");
        exec("rm {$fileName}.tex");
        exec("rm {$fileName}.out");
        exec('rm *.jpg *.png');

        if (!file_exists("{$fileName}.pdf")) {
            throw new Exception('No PDF file produced.');
        }

        return "{$fileName}.pdf";
    }
}
