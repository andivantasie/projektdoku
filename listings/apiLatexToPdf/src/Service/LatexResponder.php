<?php
declare(strict_types=1);

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class LatexResponder
 *
 * @package App\Service
 */
class LatexResponder
{
    /**
     * @return string
     * @throws GuzzleException
     */
    public function getLatex(string $host, string $cookie, int $jobId): string
    {
        $client = new Client([
            'base_uri' => "https://{$host}/job-latex/{$jobId}",
            'timeout'  => 2.0,
            'verify' => false,
            'headers' => [
                'Cookie' => "{$cookie}"
            ]
        ]);

        $response = $client->request('GET');

        return $response->getBody()->getContents();
    }
}
