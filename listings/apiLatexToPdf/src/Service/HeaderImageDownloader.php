<?php
declare(strict_types=1);

namespace App\Service;

/**
 * Class HeaderImageDownloader
 *
 * @package App\Service
 */
class HeaderImageDownloader
{
    public function download(string $host, int $jobId): void
    {
        $downloadUrl = "https://{$host}/job-header-image/{$jobId}";
        $headerImagefile = 'header-image.jpg';
        file_put_contents($headerImagefile, file_get_contents($downloadUrl));
    }
}
