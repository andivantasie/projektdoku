<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\HeaderImageDownloader;
use App\Service\LatexResponder;
use App\Service\LatexToPdfConverter;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class PdfController extends AbstractController
{
    /*
     * @param string $host
     * @param string $cookie
     * @param int $jobId
     *
     * @return Response
     * @throws GuzzleException
     * @throws Exception
     */
    public function showPdf(string $host, string $cookie, int $jobId): Response
    {
        $headerImageDownloader = new HeaderImageDownloader();
        $headerImageDownloader->download($host, $jobId);

        $latexResponder = new LatexResponder();
        $latex = $latexResponder->getLatex($host, $cookie, $jobId);

        $converter = new LatexToPdfConverter();
        $latexFileName = 'output';
        $pdf = $converter->convert($latexFileName, $latex);

        $response = new BinaryFileResponse($pdf);
        $response->deleteFileAfterSend();

        return $response;
    }
}
