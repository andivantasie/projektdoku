#!/bin/bash

clear

docker run --rm -it -v $(pwd):/home danteev/texlive latexmk -pvc -pdf main.tex
docker run --rm -it -v $(pwd):/home danteev/texlive latexmk -c

#latexmk -pvc -pdf main.tex
#latexmk -c

rm -rf main.bbl main.lol main.run.xml main.acr main.synctex.gz
